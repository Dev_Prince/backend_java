package jdbc;
import java.sql.*;
import java.util.*;
import oracle.jdbc.driver.OracleDriver;
public class Launch {
	public static void main(String[]args){
	String url="jdbc:oracle:thin:@//localhost:1521/XE";
	String un="SYSTEMhaha";
	String pw="SYSTEMjaja";
	Connection con=null;
	Statement stmt=null;
	PreparedStatement pstmt=null;
	int res=0;
	ResultSetMetaData rsmd= null;
	try {
		DriverManager.registerDriver(new OracleDriver());
		System.out.println("Driver loaded... haha now it worked");
	}
	catch(SQLException e) {
		System.out.println("Driver not loaded...");
	}
	try {
		con=DriverManager.getConnection(url,un,pw);
		System.out.println("Driver got connected...");
	}
	catch(SQLException e) {
		System.out.println("DRiver not connected...");
	}
	try {
		String q="DELETE FROM STUDENT1 WHERE USN=?";
		pstmt=con.prepareStatement(q);
		Scanner scan=new Scanner(System.in);
		System.err.println("Enter usn: ");
		String usn=scan.next();
		pstmt.setString(1,usn);
		res=pstmt.executeUpdate();
		System.out.println("Query got exe...");
	}
	catch(SQLException e) {
		System.out.println("Query not exe...");
	}
	try {
		while(res.next()) {
			String a=res.getString("name");
			String b=res.getString("usn");
			int c=res.getInt("m1");
			int d=res.getInt("m2");
			int e=res.getInt("m3");
			int f=res.getInt("m4");
			int g=res.getInt("m5");
			int h=res.getInt("m6");
			System.out.println(a+" "+b+" "+c+" "+d+" "+e+" "+f+" "+g+" "+h);
		}
	}
	catch(SQLException e){
		System.out.println("data not rendered...");
	}
}
}